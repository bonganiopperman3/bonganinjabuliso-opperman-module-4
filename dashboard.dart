// ignore: file_names
import 'package:flutter/material.dart';
import 'featureone.dart';
// ignore: unused_import
import 'package:i_recommend/profile.dart';

//2. This Code is for the Dashboard Screen after the Login and Design Implemented
class DashBoard extends StatelessWidget {
  const DashBoard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Module 4',
      //2. Theme for the screen
      theme: ThemeData(primarySwatch: Colors.yellow),
      home: Scaffold(
        appBar: AppBar(
          title: const Text("DashBoard"),
          actions: <Widget>[
            IconButton(
              onPressed: () => {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const Profile()))
              },
              icon: const Icon(
                Icons.add_to_photos_rounded,
              ),
            ),
          ],
        ),
        body: const Center(child: Text('This is a Dashboard')),
        bottomNavigationBar: FloatingActionButton(
          onPressed: () => {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => const FeatureOne()))
          },
          child: const Icon(Icons.add),
        ),
      ),
    );
  }
}
