import 'package:flutter/material.dart';
import 'package:i_recommend/dashboard.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Module 4",
      centerTitle: true,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(
          primarySwatch: Colors.yellow,
        ).copyWith(
          secondary: Colors.white,
        ),
        textTheme: const TextTheme(
          bodyText1: TextStyle(color: Colors.black),
        ),
      ),
      debugShowCheckedModeBanner: false,
      home: AnimatedSplashScreen(
        splash: Image.asset('assets/logo.png'),
      ),
      nextScreen: HomePage(),
      splashTransition: SplashTransition.fadeTransition,
      backgroundColor: Colors.yellowAccent,
    );
  }
}

//. This code is for the LOGIN screen After the Splash Animation and some Design Implemented
class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Login"),
      ),
      body: ElevatedButton(
        onPressed: () => {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const DashBoard()))
        },
        child: const Text('Login'),
      ),
    );
  }
}
